<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<style>
a[href^="#pemesanan"]{

	color: #FFFFFF;

}
.pemesanan {
	
	margin-top: 10px;
	font-family: 'Exo 2', sans-serif;
    cursor: pointer;
    border: none;
    outline: none;
    display: inline-block;
    font-size: 1em;
    padding: 10px 34px;
    background: #4CB1CA;
    color: #FFF;
    text-transform: uppercase;
    -webkit-transition: all 0.3s ease-in-out;
    -moz-transition: all 0.3s ease-in-out;
    -o-transition: all 0.3s ease-in-out;
    transition: all 0.3s ease-in-out;


}
</style>
<title>Alizea Zanns</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="{{url()}}/css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="{{url()}}/css/form.css" rel="stylesheet" type="text/css" media="all" />
<link href='//fonts.googleapis.com/css?family=Exo+2' rel='stylesheet' type='text/css'/>
<script type="text/javascript" src="{{url()}}/js/jquery1.min.js"></script>
<script src="{{url()}}/js/jquery1.min.js"></script>
<!-- start menu -->
<link href="{{url()}}/css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
<script type="text/javascript" src="{{url()}}/js/megamenu.js"></script>
<script>$(document).ready(function(){$(".megamenu").megamenu();});</script>
<script type="text/javascript" src="{{url()}}/js/jquery.jscrollpane.min.js"></script>
		<script type="text/javascript" id="sourcecode">
			$(function()
			{
				$('.scroll-pane').jScrollPane();
			});
		</script>
<!-- start details -->
<script src="{{url()}}/js/slides.min.jquery.js"></script>
   <script>
		$(function(){
			$('#products').slides({
				preload: true,
				preloadImage: 'img/loading.gif',
				effect: 'slide, fade',
				crossfade: true,
				slideSpeed: 350,
				fadeSpeed: 500,
				generateNextPrev: true,
				generatePagination: false
			});
		});
	</script>
<!--start slider -->
    <script src="{{url()}}/js/jquery-ui.min.js"></script>
    <script src="{{url()}}/js/css3-mediaqueries.js"></script>
<!--end slider -->
<script src="{{url()}}/js/jquery.easydropdown.js"></script>
</head>
<body>
     <!--<div class="header-top">
	   <div class="wrap"> 
			  <div class="header-top-left">
			  	   <div class="box">
   				      <select tabindex="4" class="dropdown">
							<option value="" class="label" value="">Language :</option>
							<option value="1">English</option>
							<option value="2">Bahasa</option>
					  </select>
   				    </div>
   				    <div class="clear"></div>
   			 </div>
			 <div class="cssmenu">
				<ul>
					<li><a href="{{route('menu',['name' => 'login'])}}">Log In</a></li> |
					<li><a href="{{route('menu',['name' => 'register'])}}">Sign Up</a></li>
				</ul>
			</div>
			<div class="clear"></div>
 		</div>
	</div>-->
	<div class="header-bottom">
	    <div class="wrap">
			<div class="header-bottom-left">
				<div class="logo">
					<a href="{{route('menu')}}"><img src="{{url()}}/images/logo_baru.png" alt=""/></a>
				</div>
				<div class="menu">
	            <ul class="megamenu skyblue">
			<li class="active grid"><a href="{{route('menu')}}">Beranda</a></li>
			<li><a class="color6" href="{{route('menu',['name' => 'produk'])}}">Produk</a></li>
			<li><a class="color4" href="#">Kategori</a>
				<div class="megapanel">
					<div class="row">
					@foreach($kategori as $row_kategori)
						<div class="col1">
							<div class="h_nav">
								<h4><a href="{{route('kategori', ['name' => 'kategori', 'id' => $row_kategori->id_kategori])}}">{{$row_kategori->nama_kategori}}</a></h4>
								<ul>
									@foreach($sub as $row_sub_kategori)
										@if($row_kategori->id_kategori == $row_sub_kategori->id_kategori)
											<li><a href="{{route('kategori', ['name' => 'sub', 'id' => $row_sub_kategori->id_sub_kategori])}}">{{$row_sub_kategori->nama_sub_kategori}}</a></li>
										@endif	
									@endforeach
								</ul>	
							</div>							
						</div>
						@endforeach	
					  </div>
					</div>
				</li>				
				<li><a class="color6" href="{{route('menu',['name' => 'tentang'])}}">Tentang Kami</a></li>
				<li><a class="color7" href="{{route('menu',['name' => 'kontak'])}}">Kontak</a></li>
			</ul>
			</div>
		</div>
	   <div class="header-bottom-right">
			 <div class="search">
				<form method="post" action="{{route('cari')}}">
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					{!! csrf_field() !!}
					<input type="text" name="cari" class="textbox" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Cari Barang';}"/>
					<input type="submit" id="submit" name="submit"/>
				</form>	
					<div id="response"> </div>
			 </div>
		</div>
     <div class="clear"></div>
     </div>
	</div>
@yield('content')
   <div class="footer">
		<div class="footer-top">
			<div class="wrap">
			  <div class="section group example">
				<div class="col_1_of_2 span_1_of_2">
					<ul class="f-list">
					  <li><img src="{{url()}}/images/2.png"><span class="f-text">Free Shipping on orders over $ 100</span><div class="clear"></div></li>
					</ul>
				</div>
				<div class="col_1_of_2 span_1_of_2">
					<ul class="f-list">
					  <li><img src="{{url()}}/images/3.png"><span class="f-text">Call us! toll free-222-555-6666 </span><div class="clear"></div></li>
					</ul>
				</div>
				<div class="clear"></div>
		      </div>
			</div>
		</div>
		<div class="footer-middle">
			<div class="wrap">		   
		   <div class="section group example">
			  <div class="col_1_of_f_1 span_1_of_f_1">
				 <div class="section group example">
				   <div class="col_1_of_f_2 span_1_of_f_2">
				      <h3>Facebook</h3>
						<script>(function(d, s, id) {
						  var js, fjs = d.getElementsByTagName(s)[0];
						  if (d.getElementById(id)) return;
						  js = d.createElement(s); js.id = id;
						  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
						  fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));</script>
						<div class="like_box">	
							<div class="fb-like-box" data-href="http://www.facebook.com/w3layouts" data-colorscheme="light" data-show-faces="true" data-header="true" data-stream="false" data-show-border="true"></div>
						</div>
 				  </div>
				  <div class="col_1_of_f_2 span_1_of_f_2">
						<h3>From Twitter</h3>
				       <div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								<p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
							</div>
							<div class="clear"> </div>
					   </div>
					   <div class="recent-tweet">
							<div class="recent-tweet-icon">
								<span> </span>
							</div>
							<div class="recent-tweet-info">
								<p>Ds which don't look even slightly believable. If you are <a href="#">going to use nibh euismod</a> tincidunt ut laoreet adipisicing</p>
							</div>
							<div class="clear"> </div>
					  </div>
				</div>
				<div class="clear"></div>
		      </div>
 			 </div>
			 <div class="col_1_of_f_1 span_1_of_f_1">
			   <div class="section group example">
				 <div class="col_1_of_f_2 span_1_of_f_2">
				    <h3>Information</h3>
						<ul class="f-list1">
						    <li><a href="#">Duis autem vel eum iriure </a></li>
				            <li><a href="#">anteposuerit litterarum formas </a></li>
				            <li><a href="#">Tduis dolore te feugait nulla</a></li>
				             <li><a href="#">Duis autem vel eum iriure </a></li>
				            <li><a href="#">anteposuerit litterarum formas </a></li>
				            <li><a href="#">Tduis dolore te feugait nulla</a></li>
			         	</ul>
 				 </div>
				 <div class="col_1_of_f_2 span_1_of_f_2">
				   <h3>Contact us</h3>
						<div class="company_address">
					                <p>500 Lorem Ipsum Dolor Sit,</p>
							   		<p>22-56-2-9 Sit Amet, Lorem,</p>
							   		<p>USA</p>
					   		<p>Phone:(00) 222 666 444</p>
					   		<p>Fax: (000) 000 00 00 0</p>
					 	 	<p>Email: <a href="mailto:info@gmail.com">mail[at]leoshop.com</a></p>
					   		
					   </div>
					   <div class="social-media">
						     <ul>
						        <li> <span class="simptip-position-bottom simptip-movable" data-tooltip="Google"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Linked in"><a href="#" target="_blank"> </a> </span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Rss"><a href="#" target="_blank"> </a></span></li>
						        <li><span class="simptip-position-bottom simptip-movable" data-tooltip="Facebook"><a href="#" target="_blank"> </a></span></li>
						    </ul>
					   </div>
				</div>
				<div class="clear"></div>
		    </div>
		   </div>
		  <div class="clear"></div>
		    </div>
		  </div>
		</div>
		<div class="footer-bottom">
			<div class="wrap">
	             <div class="copy">
			        <p>© 2016 Alizea Zanns. All rights reserved | Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>
		         </div>
				<div class="f-list2">
				 <ul>
					<li class="active"><a href="{{route('menu',['name' => 'tentang'])}}">Tentang kami</a></li> |
					<li><a href="{{route('menu',['name' => 'kontak'])}}">Kontak</a></li> 
				 </ul>
			    </div>
			    <div class="clear"></div>
		      </div>
	     </div>
	</div>
</body>
</html>