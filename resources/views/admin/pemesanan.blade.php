@extends('admin.content')
@section('content_body')
<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header" data-original-title>
						<h2><i class="halflings-icon user"></i><span class="break"></span>Daftar Pemesanan</h2>
						<div class="box-icon">
							<a href="#" class="btn-minimize"><i class="halflings-icon chevron-up"></i></a>
							<a href="#" class="btn-close"><i class="halflings-icon remove"></i></a>
						</div>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Nama Lengkap</th>
								  <th>Jumlah Pesanan</th>
								  <th>Alamat</th>
								  <th>E-mail</th>
								  <th>No. Telepon</th>
								  <th>Action</th>
							  </tr>
						  </thead>   
						  <tbody>
						  @foreach($pemesanan as $row)
							<tr>
								<td>{{$row->nama}}</td>
								<td class="center">{{$row->jumlah}}</td>
								<td class="center">{{$row->alamat}}</td>
								<td class="center">
									{{$row->email}}
								</td>
								<td class="center">{{$row->no_telepon}}</td>
								<td class="center">
									<a class="btn btn-success" href="#">
										<i class="halflings-icon white zoom-in"></i>  
									</a>
									<a class="btn btn-info" href="#">
										<i class="halflings-icon white edit"></i>  
									</a>
									<a class="btn btn-danger" href="#">
										<i class="halflings-icon white trash"></i> 
									</a>
								</td>
							</tr>
							@endforeach
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
@stop