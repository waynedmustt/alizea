@extends('body')
@section('content')
<div class="mens">    
  <div class="main">
     <div class="wrap">
		<div class="cont span_2_of_3">
		  	<h2 class="head">@if($views == 'kategori') {{$param[0]->nama_kategori}} @else {{$param[0]->nama_sub_kategori}} @endif</h2>
     	<div class="clear"></div>
	</div>
			<div class="top-box">
			@foreach($param as $row)
				 <div class="col_1_of_3 span_1_of_3"> 
				   <a href="{{route('barang',['id_kategori' => $row->id_kategori, 'id' => $row->id_barang])}}">
					<div class="inner_content clearfix">
						<div class="product_image">
							<img src="{{url()}}/{{$row->gambar}}" alt=""/>
						</div>
						<!--<div class="sale-box"><span class="on_sale title_shop">New</span></div>	-->
						<div class="price">
						   <div class="cart-left">
								<p class="title">{{$row->nama_barang}}</p>
								<div class="price1">
								  <span class="actual">{{number_format($row->harga,0,",",".")}}</span>
								</div>
							</div>
							<div class="cart-right"> </div>
							<div class="clear"></div>
						 </div>				
					   </div>
					 </a>
					</div>
				@endforeach
				<div class="clear"></div>
			</div>			 							 			    
		  </div>
			<div class="clear"></div>
			</div>
		   </div>
		</div>
		<script src="web/js/jquery.easydropdown.js"></script>
@stop