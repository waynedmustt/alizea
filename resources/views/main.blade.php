@extends('body')
@section('content')
  <!-- start slider -->
<div class="banner-slider">
				<div class="callbacks_container">
					<ul class="rslides" id="slider4">
					@foreach($slide as $row_slide)	
					    <li>
						  <div class="banner-img">
						     <img src="{{url()}}/{{$row_slide->url_gambar_slider}}" class="img-responsive" alt="" />
						  </div>
						    <h4>{{$row_slide->judul}}</h4>
						     <h5>{{$row_slide->deskripsi}}</h5>
						</li>
						@endforeach
					</ul>
			  </div>
</div>
		<!--banner Slider starts Here-->
	  	<script src="{{url()}}/js/responsiveslides.min.js"></script>
			 <script>
			    // You can also use "$(window).load(function() {"
			    $(function () {
			      // Slideshow 4
			      $("#slider4").responsiveSlides({
			        auto: true,
			        pager:false,
			        nav: true,
			        speed: 500,
			        namespace: "callbacks",
			        before: function () {
			          $('.events').append("<li>before event fired.</li>");
			        },
			        after: function () {
			          $('.events').append("<li>after event fired.</li>");
			        }
			      });
			
			    });
			  </script>

    <!--/slider -->
<div class="main">
	<div class="wrap">
		<div class="section group">
		  <div class="cont span_2_of_3">	
		  <h2 class="head">Daftar Produk</h2>
		  <div class="top-box1">
			@foreach($barang as $row)
				<div class="col_1_of_3 span_1_of_3">
					 <a href="{{route('barang',['id_kategori' => $row->id_kategori, 'id' => $row->id_barang])}}">
					<div class="inner_content clearfix">
					<div class="product_image">
						<img src="{{url()}}/{{$row->gambar}}" alt=""/>
					</div>
				    <div class="price">
					   <div class="cart-left">
							<p class="title">{{$row->nama_barang}}</p>
							<div class="price1">
							  <span class="actual">Rp. {{number_format($row->harga,0,",",".")}}</span>
							</div>
						</div>
						<div class="cart-right"> </div>
						<div class="clear"></div>
					 </div>				
                   </div>
                   </a>
				</div>
				@endforeach
				<div class="clear"></div>
			</div>				 						 			    
		  </div>
			<div class="rsidebar span_1_of_left">
				<div class="top-border"> </div>
				 <div class="border">
	             <link href="{{url()}}/css/default.css" rel="stylesheet" type="text/css" media="all" />
	             <link href="{{url()}}/css/nivo-slider.css" rel="stylesheet" type="text/css" media="all" />
				  <script src="{{url()}}/js/jquery.nivo.slider.js"></script>
				    <script type="text/javascript">
				    $(window).load(function() {
				        $('#slider').nivoSlider();
				    });
				    </script>
		    <div class="slider-wrapper theme-default">
              <div id="slider" class="nivoSlider">
				@foreach($barang_terbaru as $row_terbaru)
					<img src="{{url()}}/{{$row_terbaru->gambar}}"  alt="" />
				@endforeach	
              </div>
             </div>
              <div class="btn"><a href="{{route('menu',['name' => 'produk'])}}">Check it Out</a></div>
             </div>
           <!--<div class="top-border"> </div>
			<div class="sidebar-bottom">
			    <h2 class="m_1">Newsletters<br> Signup</h2>
			    <p class="m_text">Lorem ipsum dolor sit amet, consectetuer</p>
			    <div class="subscribe">
					 <form>
					    <input name="userName" type="text" class="textbox">
					    <input type="submit" value="Subscribe">
					 </form>
	  			</div>
			</div>-->
	    </div>
	   <div class="clear"></div>
	</div>
	</div>
	</div>
@stop