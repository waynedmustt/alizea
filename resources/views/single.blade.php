@extends('body')
@section('content')
<div class="mens">    
  <div class="main">
     <div class="wrap">
     	<ul class="breadcrumb breadcrumb__t"><a class="home" href="{{route('menu')}}">Beranda</a> / <a href="{{route('kategori',['name' => 'kategori','id' => $barang[0]->id_kategori])}}">{{$barang[0]->nama_kategori}}</a> / <a href="{{route('kategori',['name' => 'sub','id' => $barang[0]->id_sub_kategori])}}">{{$barang[0]->nama_sub_kategori}} </a> / {{$barang[0]->nama_barang}}</ul>
		<div class="cont span_2_of_3">
		  	<div class="grid images_3_of_2">
						<!-- FlexSlider -->
						<script src="{{url()}}/js/imagezoom.js"></script>
							<script defer src="{{url()}}/js/jquery.flexslider.js"></script>
							<link rel="stylesheet" href="{{url()}}/css/flexslider.css" type="text/css" media="screen" />
							<script>
							// Can also be used with $(document).ready()
								$(window).load(function() {
									$('.flexslider').flexslider({
										animation: "slide",
										controlNav: "thumbnails"
									 });
								});
							</script>
						<!-- //FlexSlider-->
						<div class="flexslider">
							  <ul class="slides">
							  @foreach($barang as $row_barang)
								<li data-thumb="{{url()}}/{{$row_barang->gambar}}">
									<div class="thumb-image"> <img src="{{url()}}/{{$row_barang->gambar}}" data-imagezoom="true" class="img-responsive"> </div>
								</li>
							  @endforeach	
							  </ul>
							<div class="clearfix"></div>
					</div>		



	            </div>
		         <div class="desc1 span_3_of_2">
		         	<h3 class="m_3">{{$barang[0]->nama_barang}}</h3>
		             <p class="m_5">Rp. {{number_format($barang[0]->harga,0,",",".")}} </p>
		         	 <div class="pemesanan">
						
							<a href="#pemesanan">Pesan</a>
						
						<script>
							$(document).ready(function(){
							
							//scroll hash
							$('a[href^="#"]').on('click',function (e) {
								e.preventDefault();

								var target = this.hash;
								var $target = $(target);

								$('html, body').stop().animate({
									'scrollTop': $target.offset().top
								}, 900, 'swing', function () {
									window.location.hash = target;
								});
							});
							
							});
						</script>
					 </div>
				     <p class="m_text2">{{$barang[0]->deskripsi}}</p>
			     </div>
			   <div class="clear"></div>	
	    <div class="clients">
	    <h3 class="m_3">{{count($barang_satu_kategori)}} Produk dalam Satu Kategori</h3>
		 <ul id="flexiselDemo3">
		   @foreach($barang_satu_kategori as $row_satu_kategori)
				<li><img src="{{url()}}/{{$row_satu_kategori->gambar}}" /><a href="#">{{$row_satu_kategori->nama_barang}}</a><p>{{number_format($row_satu_kategori->harga,0,",",".")}}</p></li>
			@endforeach
		 </ul>
	<script type="text/javascript">
		$(window).load(function() {
			$("#flexiselDemo1").flexisel();
			$("#flexiselDemo2").flexisel({
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });
		
			$("#flexiselDemo3").flexisel({
				visibleItems: 5,
				animationSpeed: 1000,
				autoPlay: false,
				autoPlaySpeed: 3000,    		
				pauseOnHover: true,
				enableResponsiveBreakpoints: true,
		    	responsiveBreakpoints: { 
		    		portrait: { 
		    			changePoint:480,
		    			visibleItems: 1
		    		}, 
		    		landscape: { 
		    			changePoint:640,
		    			visibleItems: 2
		    		},
		    		tablet: { 
		    			changePoint:768,
		    			visibleItems: 3
		    		}
		    	}
		    });
		    
		});
	</script>
	<script type="text/javascript" src="{{url()}}/js/jquery.flexisel.js"></script>
     </div>
     <div class="toogle">
		<div class="register_account" id="pemesanan">
          	<div class="wrap">
			@if(session('message'))
				{{session('message')}}
			@endif
				<h3 class="m_3">Pemesanan</h3>
				<form method="post" action="{{route('pemesanan')}}">
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					{!! csrf_field() !!}
						 <div class="col_1_of_2 span_1_of_2">
						 <div><input type="hidden" name="id_barang" value="{{$barang[0]->id_barang}}"/></div>
						 <div><input type="hidden" name="id_kategori" value="{{$barang[0]->id_kategori}}"/></div>
							 <div><input type="text" name="nama" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Nama Lengkap';}" placeholder="Nama Lengkap"/></div>
								<div><input type="text" name="jumlah" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Jumlah Pesanan';}" placeholder="Jumlah Pesanan"/></div>
								<div><input type="text" name="email" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'E-Mail';}" placeholder="E-mail"/></div>
							<div><input type="text" name="alamat" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Alamat';}" placeholder="Alamat"/></div>		        
						  <div><input type="text" name="kota" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Kota';}" placeholder="Kota"/></div>
							<input type="text" name="no_telepon" value="" placeholder="No. Telepon"/>
						  </div>
					<div class="col_1_of_2 span_1_of_2">	
					  <button class="grey">Pesan</button>
					</div>  
					<div class="clear"></div>
				  </form>
			</div>
		</div>		
     </div>
      </div>
			 <div class="clear"></div>
		   </div>
		</div>
</div>
@stop