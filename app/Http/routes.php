<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//E-COMMERCE
Route::group(array('prefix' => '/'), function()
{
	Route::get('{name?}',['as' => 'menu', 'uses' => 'MenuController@index']);
	Route::get('{name}/{id}',['as' => 'kategori', 'uses' => 'KategoriController@index']);
	
});

Route::group(array('prefix' => 'barang'), function()
{
	Route::get('{id_kategori}/{id}',['as' => 'barang', 'uses' => 'BarangController@index']);
	Route::post('/',['as' => 'cari', 'uses' => 'CariController@index']);
	
});

Route::group(array('prefix' => 'pemesanan'), function()
{
	Route::post('/',['as' => 'pemesanan', 'uses' => 'PemesananController@index']);
	
});

//ADMIN
Route::group(array('prefix' => 'admin'), function()
{
	Route::get('login/alizea',['as' => 'admin', 'uses' => 'AdminController@index']);
	Route::post('login/alizea',['as' => 'login', 'uses' => 'AdminController@login']);
	Route::get('beranda/alizea',['as' => 'beranda', 'uses' => 'AdminController@login']);
	Route::get('logout/alizea',['as' => 'logout', 'uses' => 'AdminController@logout']);
	
	Route::get('profil/alizea',['as' => 'profil', 'uses' => 'ProfilController@index']);
	
	Route::get('pemesanan/alizea',['as' => 'list_pemesanan', 'uses' => 'AdminPemesananController@index']);
	
});
