<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Util\Kategori;

class BarangController extends Controller {

	public function index($id_kategori,$id){
	
		$kategori = Kategori::get();
		
		$sub = Kategori::getSub();
		
		$barang = DB::table('barang')
				->join('jenis','jenis.id_kategori','=','barang.id_kategori')
				->join('sub_kategori','sub_kategori.id_sub_kategori','=','barang.id_sub_kategori')
				->where('barang.id_barang',$id)
				->get();
				
		$barang_satu_kategori = DB::table('barang')
				->join('jenis','jenis.id_kategori','=','barang.id_kategori')
				->where('barang.id_barang','!=',$id)
				->where('jenis.id_kategori',$id_kategori)
				->take(10)
				->get();
		
		if(!empty($barang)){
		
			return view('single',compact('barang_satu_kategori','barang','kategori','sub'))->render();
		
		} else {
		
			return view('404',compact('kategori','sub'))->render();
		
		}
		
	}


}