<?php 

namespace App\Http\Controllers\Util;

use DB;

class Kategori {
	
	public static function get($id = null){
		
		if($id == null) {
		
			$kategori = DB::table('jenis')
					->get();
				
		} else {
		
			$kategori = DB::table('jenis')
					->join('barang','barang.id_kategori','=','jenis.id_kategori')
					->where('jenis.id_kategori',$id)
					->get();
		
		}
		
		return $kategori;
	}
	
	public static function getSub($id = null){
	
		if($id == null){
		
			$sub = DB::table('sub_kategori')
				->get();
		
		} else {
		
			$sub = DB::table('sub_kategori')
				->join('barang','barang.id_sub_kategori','=','sub_kategori.id_sub_kategori')
				->where('sub_kategori.id_sub_kategori',$id)
				->get();
		
		}
				
		return $sub;
	
	}

}