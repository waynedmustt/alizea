<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

class AdminPemesananController extends Controller {

	public function index(){
		
		if(session()->has('username')){
		
			$pemesanan = DB::table('pemesanan')
						->orderBy('created_at','DESC')
						->get();
		
			return view('admin/pemesanan',compact('pemesanan'))->render();
		
		} else {
		
			return redirect('admin/login/alizea')->with('message','login terlebih dahulu');
		
		}
		
	}
	
}