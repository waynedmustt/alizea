<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

class AdminController extends Controller {

	public function index(){
		//print_r(session('username'));
		if(session()->has('username')){
		
			return view('admin/index');
		
		} else {
		
			session()->push('view','login');
			
			return view('admin/login');
		
		}
		
	}
	
	public function logout(){
	
		session()->flush();
		
		session()->forget('username');
	
		return redirect('admin/login/alizea');
	
	}
	
	public function login() {
	
		if(!session()->has('username')){
		
			if(isset($_POST['username']) && isset($_POST['password'])) {
			
				$username = $_POST['username'];
			
				$password = $_POST['password'];
				
				$user = DB::table('user')->get();
				
				foreach($user as $row){
				
					if($row->password == $password || $row->username == $username){
					
						session()->forget('view');
					
						session()->push('username',$username);
						
						return view('admin/index');
					
					} else {
					
						return redirect('admin/login/alizea')->with('message','Username atau Password Salah');
					
					}
				
				}
				
			
			} else {
			
				return view('admin/index');
			
			}
		
		} else {
		
			return view('admin/index');
		
		}
	
	}

}