<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Util\Kategori;

class KategoriController extends Controller {

	public function view($views,$param,$kategori,$sub){
	
		if(empty($param)){
		
			return view('404',compact('kategori','sub'))->render();
		
		} else {
		
			return view('kategori',compact('views','param','kategori','sub'))->render();
		
		}
	
	}

	public function index($name,$id){
	
		$kategori = Kategori::get();
		
		$sub = Kategori::getSub();
		
		switch($name) { 
		
			case 'kategori':
		
				$kategori_detail = Kategori::get($id);
				
				print_r($this->view('kategori',$kategori_detail,$kategori,$sub));
				
			break;
			
			case 'sub':
		
				$sub_detail = Kategori::getSub($id);
			
				print_r($this->view('sub',$sub_detail,$kategori,$sub));
			
			break;
		
			default:
			
				$param = '';
		
				$this->view($param,$kategori,$sub);
		
			break;
			
		}
		
	}


}