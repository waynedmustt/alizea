<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;

class ProfilController extends Controller {

	public function index() {

		if(session()->has('username')){
			
				return view('admin/profil');
			
			} else {
			
				return redirect('admin/login/alizea')->with('message','login terlebih dahulu');
			
			}
		
	}
}