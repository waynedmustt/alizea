<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Util\Kategori;

class CariController extends Controller {

	public function index($name = null){
	
		$kategori = Kategori::get();
		
		$sub = Kategori::getSub();
		
		$yang_dicari = $_POST['cari'];
		
		$barang = DB::select("select * from barang where nama_barang LIKE '%$yang_dicari%'");

		return view('cari',compact('barang','kategori','sub'))->render();
		
	}

}