<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Util\Kategori;

class PemesananController extends Controller {

	public function index(){
	
		$kategori = Kategori::get();
		
		$sub = Kategori::getSub();
		
		$nama = $_POST['nama'];
		
		$id_barang = $_POST['id_barang'];
		
		$id_kategori = $_POST['id_kategori'];
		
		$jumlah = $_POST['jumlah'];
		
		$email = $_POST['email'];
		
		$alamat = $_POST['alamat'];
		
		$kota = $_POST['kota'];
		
		$no_telepon = $_POST['no_telepon'];
		
		$pemesanan = DB::table('pemesanan')
				->insertGetId([
				'id_barang' => $id_barang,
				'nama' => $nama,
				'jumlah' => $jumlah,
				'email' => $email,
				'alamat' => $alamat,
				'kota' => $kota,
				'no_telepon' => $no_telepon,
				'created_at' => date("Y-m-d h:i:sa"),
				'deleted_at' => NULL,
				'updated_at' => NULL
				]);

		return redirect("barang/$id_kategori/$id_barang")->with('message','Pemesanan Berhasil Dilakukan!');			

		//return view('cari',compact('barang','kategori','sub'))->render();
		
	}

}