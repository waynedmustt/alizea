<?php 

namespace App\Http\Controllers;

use DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Util\Kategori;

class MenuController extends Controller {

	public function index($name = null){
	
		$kategori = Kategori::get();
		
		$sub = Kategori::getSub();
	
		if($name != null){
		
			switch($name){
			
				case "tentang":
				
					return view('about',compact('kategori','sub'))->render();
					
				break;
				
				case "kontak":
				
					return view('contact',compact('kategori','sub'))->render();
				
				break;
				
				case "produk":
				
					$barang = DB::table('barang')->get();
				
					return view($name,compact('barang','kategori','sub'))->render();
				
				break;
				
				default:
				
					return view('404',compact('kategori','sub'))->render();
				
				break;
			
			}
		
		
		} else {
		
			$barang = DB::table('barang')
						->orderBy('created_at','ASC')
						->take(12)
						->get();
			
			$barang_terbaru = DB::table('barang')
							->orderBy('created_at','ASC')
							->take(3)
							->get();
			
			$slide = DB::table('slide')->get();
		
			return view('main',compact('barang_terbaru','barang','slide','kategori','sub'))->render();
		
		}
		
	}


}